### 0 clone the repository
`git clone https://@bitbucket.org:Stasyanz/linkedin-alsoviewed.git`
### 1 pip install -r requirements.txt

### 2 install google chrome

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt-get -y update && apt-get install -y python3-pip bash -yqq unzip && apt-get install -y --allow-unauthenticated google-chrome-stable

### 3 install chromedriver

wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
sudo unzip /tmp/chromedriver.zip chromedriver -d /usr/bin/
sudo chmod +x /usr/bin/chromedriver

### 4 
set `email` and `password` in `input.json`. Also set `url` you want to scrape (public profile)

### 5
 run `python main.py`

### 6 find results in result.txt

*Important**
if you logging in for the first time of from a new (for LinkedIn) IP , you probably will be asked to enter security 
code. If you see in console `Enter your code:`, enter the code you received from LenkedIn and press `Enter`. 
You have 30 seconds to do that