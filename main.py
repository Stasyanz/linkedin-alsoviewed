from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import sys, os
import json


sys.path.append(os.getcwd())
chrome_driver = "/usr/bin/chromedriver"


class Bot():

    def __init__(self):
        with open("input.json") as f:
            data = json.load(f)
            self.url = data["url"]
            self.email = data["email"]
            self.password = data["password"]
        if not all([len(self.url)>0, len(self.email)>0, len(self.password)>0]):
            print("Empty value in input.json")
            exit(0)

        self.options = Options()
        self.options.add_argument('--disable-browser-side-navigation')
        self.options.add_argument("--ignore-ssl-errors=yes")
        self.options.add_argument("--headless")
        self.options.add_argument("--window-size=1920,1080")
        self.options.add_argument("--no-sandbox")
        self.options.add_argument("--disable-extensions")
        self.options.add_argument("--disable-gpu")
        self.options.add_argument("--disable-dev-shm-usage")
        self.browser = webdriver.Chrome(executable_path=chrome_driver, options=self.options)

    def signIn(self):
        print("Signing in")
        try:
            emailInput = self.browser.find_element_by_xpath("//input[contains(@id, 'username')]")
            emailInput.clear()
            passwordInput = self.browser.find_element_by_xpath("//input[contains(@id, 'password')]")
            passwordInput.clear()
        except:
            emailInput = self.browser.find_element_by_xpath("//input[contains(@id, 'login-email')]")
            emailInput.clear()
            passwordInput = self.browser.find_element_by_xpath("//input[contains(@id, 'login-password')]")
            passwordInput.clear()
        if emailInput:
            emailInput.send_keys(self.email)
            passwordInput.send_keys(self.password)
            time.sleep(1)
            passwordInput.send_keys(Keys.ENTER)
            try:
                self.browser.find_element_by_xpath('.//button[text()="Not Now"]').click()
                time.sleep(1)
            except:
                pass
        time.sleep(1)
        try:
            codeInput = self.browser.find_element_by_xpath("//input[contains(@id, 'input__email_verification_pin')]")
            code = input("Enter your code:")
            time.sleep(30)
            codeInput.send_keys(code)

            self.browser.find_element_by_xpath("//button{contains(@id, 'email-pin-submit-button')}").click()
        except:
            pass
        with open('cookies_{}.txt'.format(self.email), 'w') as f:
            c = self.browser.get_cookies()
            f.write(json.dumps(c))
            print("Wrote new cookies")

    def set_cookas(self, cookies):
        for cookie in cookies:
            self.browser.add_cookie(cookie)

    def go(self, url, cookies):
        self.browser.get(self.url)
        if len(cookies) > 0:
            self.set_cookas(cookies)
        try:
            form_toggle = self.browser.find_element_by_xpath("//a[contains(@class,'form-toggle') and text() = 'Sign in']")
            print("Login form appeared")
            form_toggle.click()
            time.sleep(2)
            self.signIn()
        except Exception as e:
            print("Error: {}".format(e))
            pass
        try:
            self.browser.find_element_by_xpath("//*[@id='username']")
            self.signIn()
        except:
            pass
        self.browser.get(url)
        time.sleep(3)
        links = []
        try:
            section = self.browser.find_element_by_xpath("//ul[contains(@class, 'mini-profiles-list')]")
            lis = section.find_elements_by_xpath(".//a[contains(@class, 'mini-profile--link')]")
        except:
            section = self.browser.find_element_by_xpath("//ul[contains(@class, 'pv-profile-section__section-info section-info browsemap mt4')]")
            lis = section.find_elements_by_xpath(".//a[contains(@class, 'pv-browsemap-section__member')]")
        print("People found: {}".format(len(lis)))
        for li in lis:
            href = li.get_attribute("href")
            head, sep, tail = href.partition('?')
            links.append(head)
        with open("result.txt", 'w') as f:
            for link in links:
                f.write(link+os.linesep)

def main():
    bot = Bot()
    """try to read cookies"""
    try:
        f = open('cookies_{}.txt'.format(bot.email), 'r')
        cookies = json.loads(f.readline())
        f.close()
    except:
        cookies = []
    bot.go(bot.url, cookies)


if __name__ == '__main__':
    main()
